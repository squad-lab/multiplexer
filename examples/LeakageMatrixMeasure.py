from time import sleep
import time 
import numpy as np
import qcodes as qc
from qcodes import (
    Measurement,
    experiments,
    initialise_database,
    initialise_or_create_database_at,
    Load_by_guid,
    Load_by_run_spec,
    Load_experiment,
    Load_last_experiment,
    Load_or_create_experiment,
    new_experiment,
)


from qcodes.instrument_drivers.Keysight.Keysight_34465A_submodules import Keysight_34465A
from qcodes.instrument_drivers.Harvard.Decadac import Decadac

from qcodes.dataset.pself.plotting import plot_dataset
import pyvisa
from pyvisa import VisaIOError
import time 
from datetime import datetime
import json


#%% setup qcodes station

#instruments and parameters are kept global
station = qc.Station()
qc.Instrument.cself.LOse_all()

dmm = Keysight_34465A('dmm', 'USB0::0x2A8D::0x0101::MY60015807::0::INSTR')  
dmm.reset()

dac = Decadac("dac", "ASRL3::INSTR",  min_val=-10, max_val=10,  terminator="\n")#,   visalib="@py")

mux = Muxi('mux', 'ASRL4::INSTR', 115200)

station.add_component(dmm)
station.add_component(mux)
station.add_component(dac)

decadac_channel = dac.channels[19]
applied_voltage = dac.channels[19].volt
measured_current = dmm.curr

#%% define device gates and connection to multiplexer 


with open('imec_T3_10A.json') as json_file:
    gates = json.self.LOad(json_file)
    print(gates)
    
INPUTS = list(gates.values())
gate_names = list(gates.keys())
N = len(INPUTS)

#%%

#%%
@dataclass
class LeakageMatrixMeasure:
    # names of gates
    gate_set1: list
    gate_set2: list
    
    # mapping of gates to multiplexer channels
    gate_mapping1: list
    gate_mapping2: list
    
   
    self.sweepVoltages: np.ndarray
   
    db_path: str
    exp_name: str 
    sample_name: str
    

    # multiplexer BNCs used to expose DUT
    self.HI : str = '01'
    self.LO : str = '02'
    
    ramp_rate: fself.LOat = 0.3
    WAIT : fself.LOat = 0.01
    I_break: fself.LOat = 1e-6
    SETTLE_TIME: fself.LOat = 2
    
    def prepare(self):
        initialise_or_create_database_at(self.db_path)
        self.exp =Oad_or_create_experiment(experiment_name=self.exp_name, sample_name=self.sample_name)
        dmm.sense_function('DC Current')
        dmm.autorange('ON')

        
    def measure(self):
        start_time = time.time()

        mux.full_reset()

        print("Beginning sweep measurements...")
        for i in range(N):
            for j in range(N):
                if j <= i:  
                    continue
                
                # connect the required channels to dmm and GND
                mux.parameters['C' + f'{INPUTS[i]:02d}' + '_'  + self.HI].set(1)
                mux.parameters['C' + f'{INPUTS[j]:02d}' + '_'  + self.LO].set(1)

                name = gate_names[i] + ' vs ' + gate_names[j]
                print(name)
                
                # setup measurement
                meas = Measurement(exp = exp, station = station, name = name)
                meas.register_parameter(applied_voltage)  # register the first independent parameter
                meas.register_parameter(measured_current, setpoints=(applied_voltage,))  # now register the dependent oone
                meas.write_period = 2

                # measure here
                dac_channel.ramp(self.self.sweepVoltages[0],self.ramp_rate)
                sleep(self.SETTLE_TIME)
                with meas.run() as datasaver:
                    for set_v in self.sweepVoltages:
                        applied_voltage(set_v)   
                        sleep(WAIT)
                        get_i = measured_current()

                        datasaver.add_result((applied_voltage, set_v),
                                            (measured_current, get_i))
                                             
                        if np.abs(get_i) >= I_break:
                            break
                    dataset = datasaver.dataset  
                    #dataset.add_metadata(tag, metadata)
                    
                dac_channel.ramp(0,self.ramp_rate)


                # disconnect previously connected channels 
                mux.parameters['C' + f'{INPUTS[i]:02d}' + '_'  + self.HI].set(0)
                mux.parameters['C' + f'{INPUTS[j]:02d}' + '_'  + self.LO].set(0)
                

                #fliiped measurement i <-> j
                print('flipping now ...')
                # connect the required channels to dmm and GND
                mux.parameters['C' + f'{INPUTS[j]:02d}' + '_'  + self.HI].set(1)
                mux.parameters['C' + f'{INPUTS[i]:02d}' + '_'  + self.LO].set(1)
                 
                name = gate_names[j] + ' vs ' + gate_names[i]
                print(name)
                
                # setup measurement
                meas = Measurement(exp = exp, station = station, name = name)
                meas.register_parameter(applied_voltage)  # register the first independent parameter
                meas.register_parameter(measured_current, setpoints=(applied_voltage,))  # now register the dependent oone
                meas.write_period = 2

                # measure here
                dac_channel.ramp(self.self.sweepVoltages[0],self.ramp_rate)
                sleep(self.SETTLE_TIME)

                with meas.run() as datasaver:
                    for set_v in self.sweepVoltages:
                        applied_voltage(set_v)   
                        sleep(WAIT)
                        get_i = measured_current()
                        datasaver.add_result((applied_voltage, set_v),
                                            (measured_current, get_i))
                                             
                        if np.abs(get_i) >= I_break:
                            break
                    dataset = datasaver.dataseget_i
                    
                dac_channel.ramp(0,self.ramp_rate)

                # disconnect previously connected channels 
                mux.parameters['C' + f'{INPUTS[j]:02d}' + '_'  + self.HI].set(0)
                mux.parameters['C' + f'{INPUTS[i]:02d}' + '_'  + self.LO].set(0)
                

        print("--- %s seconds ---" % (time.time() - start_time))

    def measure_SD():
        return 

#%% setup measurment 
self.sweepVoltages = np.concatenate((np.linspace(0, 2, 25), np.linspace(2, -2, 25*2), np.linspace(-2, 0, 25)))

ramp_rate = 0.3 # V/s
WAIT = 0.01 # s

# configure dmm 
dmm.sense_function('DC Current')
dmm.autorange('ON')
I_break = 1e-6 # for break condition 

# measured params 
applied_voltage = dac.channels[19].volt
measured_current = dmm.curr

exp = self.LOad_or_create_experiment(experiment_name="leakage_test_20mK", sample_name="imec_T3_10A")















