#from qtools.instrument.custom_drivers.ZI.MFLI import MFLI
import time
import numpy as np

from qcodes.instrument_drivers.stanford_research.SR830 import SR830
from qcodes.instrument_drivers.tektronix.Keithley_2400 import Keithley_2400

import os
import pyvisa 

import matplotlib.pyplot as plt
from tqdm import tqdm

from itertools import repeat


#%% Initialize instruments and arrays

lockin = SR830("lockin",'GPIB1::12::INSTR')
keithley=Keithley_2400('keithley', 'GPIB1::20::INSTR')
mux = Muxi('mux', 'ASRL10::INSTR', 115200)

keithley.output.set(1)
lockin.frequency.set(10)
lockin.time_constant.set(0.1)
lockin.amplitude.set(0.1)
keithley.compliancei.set(1e-3)
C_arr = []
R_arr = []

#%% Measure RC without mux
while True:
    I = keithley.curr.get()
    U = keithley.volt.get()
    R_arr.append(U/I)
    print(U/I)
    
    f = lockin.frequency.get()
    R_diff = 10000
    V_amp = lockin.amplitude.get()
    V_diff = lockin.R.get()
    
    C = (V_diff/R_diff)/(2*np.pi*V_amp*f)
    print(C)
    C_arr.append(C)
    time.sleep(10)

#%% Save arrays
directory = 'Z:/Visser/write/RTFASSDE/'
file_name = 'R_C_4K_10s_1.csv'
np.savetxt(directory+file_name,[R_arr,C_arr],delimiter = ',')


#%% Measurement with multiplexer
def cap_measurement(lockin,smu,mux,lockin_chan,smu_chan,res_chans,cap_chans,directory, file_name,R_diff=1e4,wait_time = 10,plot = True):
    
    #Prepare plotting windows
    if plot:
        fig1, axr = plt.subplots(constrained_layout=True)
        axc = axr.twinx()
        axr.set_xlabel('Time (s)')
        axr.set_ylabel(r'Resistance ($\Omega$)')
        axc.set_ylabel('Capacitance (nF)')
        fig2, axrc = plt.subplots(constrained_layout=True)
    
    R_arr = [[] for i in repeat(None, len(res_chans))]
    C_arr = [[] for i in repeat(None, len(cap_chans))]
    t_arr = []
    t0 = time.time()

    try:
        while True:    
            for i in range(len(res_chans)):
                mux.parameters['C' + f'{res_chans[i]:02d}' + '_'  + f'{smu_chan:02d}'].set(1)
                I = smu.curr.get()
                U = smu.volt.get()
                R_arr[i].append(U/I)
                mux.parameters['C' + f'{res_chans[i]:02d}' + '_'  + f'{smu_chan:02d}'].set(0)

            for i in range(len(cap_chans)):
                mux.parameters['C' + f'{cap_chans[i]:02d}' + '_'  + f'{lockin_chan:02d}'].set(1)

                f = lockin.frequency.get()
                V_amp = lockin.amplitude.get()
                V_diff = lockin.R.get()
                
                C = (V_diff/R_diff)/(2*np.pi*V_amp*f)
                C_arr[i].append(C)
                mux.parameters['C' + f'{cap_chans[i]:02d}' + '_'  + f'{lockin_chan:02d}'].set(0)

            
            t_arr.append(time.time()-t0)
            time.sleep(wait_time)
            if plot:
                for i in range(len(res_chans)):
                    axr.plot(R_arr)
                for i in range(len(cap_chans)):
                    axc.plot(C_arr[i])
                    print(R_arr)
                    print(C_arr)
                    axrc.plot(R_arr[0],C_arr[i])
                    
                plt.pause(0.1)
                
    except KeyboardInterrupt:
        np.savetxt(directory+file_name,[*R_arr,*C_arr,t_arr],delimiter = ',')
        
#%% Run measurement
directory = 'Z:/Visser/write/RTFASSDE/'
file_name = 'R_C_4K_10s_2.csv'
cap_measurement(lockin,keithley,mux,1,2,[1,2],[3,4],directory, file_name,R_diff=1e4,wait_time = 10,plot = True)
    