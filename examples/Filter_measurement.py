
#from qtools.instrument.custom_drivers.ZI.MFLI import MFLI
import time
import json
import numpy as np

from qcodes.instrument_drivers.stanford_research.SR830 import SR830
from qcodes.instrument_drivers.tektronix.Keithley_2400 import Keithley_2400

import os
import pyvisa 

import matplotlib.pyplot as plt
from tqdm import tqdm

#%% Initialize instruments and arrays

lockin = SR830("lockin_up",'GPIB1::12::INSTR')
mux = Muxi('mux', 'ASRL10::INSTR', 115200)
lockin.time_constant.set(0.1)
lockin.amplitude.set(1)

#%% Read chainer mapping
mapping = []
with open('Z:\Visser\write\RTFASSDE\map_chainer.json', 'r') as map_file:
    mapping = json.loads(map_file.read())

def map_chan(chan, m=mapping, rev=False):
    r_map = {str(v):int(k) for (k,v) in m.items()}
    if m and str(chan) in m:
        if rev:
            return int(r_map[str(chan)])
        else:
            return int(m[str(chan)])
#%% Frequency sweep

f = np.linspace(100,102e3,10)
lines = []
offset = 50

for mux_chan in range(1,5):
    mux.full_reset()
    print(f'Now measuring from {mux_chan} (mapped: {map_chan(mux_chan)}) to {mux_chan+offset} (mapped: {map_chan(mux_chan+offset,rev=True)})')
    mux.parameters['C' + f'{map_chan(mux_chan,rev=True):02d}' + '_'  + '01'].set(1)
    mux.parameters['C' + f'{(map_chan(mux_chan+offset,rev=True)):02d}' + '_'  + '03'].set(1)   
    
    R_arr = []
    for frequeny in f:
        lockin.frequency.set(frequeny)
        time.sleep(1)
        R = lockin.R.get()
        print(R)
        R_arr.append(R)
    lines.append(R_arr)
    
    
    
#%% Frequency sweep with plotting

def frequency_sweep(lockin,mux,mux_chans, mux_offset,frequencies, input_amplitude, directory, file_name ,time_constant=0.1,plot = True):
    # Prepare live plotting
    try:
        if plot: 
            plt.figure()
            plt.xlabel('Frequency (Hz)')
            plt.ylabel(r'Normalized amplitude $A/A_0$')
            plt.tight_layout()
        
        #Prepare lock-in settings
        lockin.time_constant.set(time_constant)
        lockin.amplitude.set(input_amplitude) 
        amp_arr_all = np.zeros((len(mux_chans),len(frequencies)))
        counter = 0
        for mux_chan in mux_chans:
            mux.full_reset()
            print(f'Now measuring from {mux_chan} (mapped: {map_chan(mux_chan,rev = True)}) to {mux_chan+mux_offset} (mapped: {map_chan(mux_chan+mux_offset,rev=True)})')
            mux.parameters['C' + f'{map_chan(mux_chan,rev=True):02d}' + '_'  + '01'].set(1)
            mux.parameters['C' + f'{(map_chan(mux_chan+mux_offset,rev=True)):02d}' + '_'  + '03'].set(1)   
            
            amp_arr = []
            for frequency in frequencies:
                lockin.frequency.set(frequency)
                time.sleep(3*time_constant)
                amp = lockin.R.get()
                amp_arr.append(amp)
            amp_arr_all[counter] = amp_arr
            if plot:
                plt.plot(frequencies,amp_arr,label = str(mux_chan))
                plt.pause(0.3)
    except KeyboardInterrupt:
        np.savetxt(directory+file_name,[*amp_arr_all],delimiter = ',')
#%% Execute measurement
directory = 'Z:/Visser/write/RTFASSDE/'
file_name = 'Filter_meas_1.csv'
mux_chans = np.linspace(1,48,48,dtype = int)
f = np.linspace(100,102e3,20)
frequency_sweep(lockin,mux,mux_chans,50,f,0.1,directory, file_name) 

    
    
    
    
    
    
    
    
    
    
    
    