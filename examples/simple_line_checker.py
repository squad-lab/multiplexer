#!/usr/bin/env python3
from IPython import embed
import argparse
from argparse import RawDescriptionHelpFormatter
import time
import datetime
import json
import csv
import io
import logging

import serial
import serial.tools.list_ports
from blessed import Terminal
from packaging import version

# Mux communication settings
BAUDRATE=115200
READ_TERM = '\r\n'
READ_TIMEOUT = 60
WRITE_TERM = '\n'

class FirmwareError(Exception):
    """Raised if firmware is too old."""
    def __init__(self, curr_ver, req_ver, message="Mux firmware update required"):
        self.curr_ver = curr_ver
        self.req_ver = req_ver
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return f'Current: {self.curr_ver}, Required: {self.req_ver}. {self.message}'

#TODO: Expand explanation of measurement principle on webpage
def main():
    con_schema = """1. Ground BNC channel GND_CHAN
2. Attach resistor SENS_RES between ADC and DAC channels
3. Attach probe to ADC
4. Measure resistance from probe to any input pin on D-SUB connector via DUT.
BNC ordering (left to right, top to bottom): 1,2,3,4,ADC,DAC.
     +---------/\----------+
    / pin 100 /B/ pin 50  /|
   /         /U/         / |
  /         /S/         /  |
 /  pin 51 /D/ pin 1   /   +
+----------\/---------+   /
|          1(o)   2(o)|  /
| /---\    3(o)   4(o)| /
| |USB|  ADC(o) DAC(o)|/
+-\---/---------------+"""
    out_help = """outputs a sorted pin:resistance list for all input pins 1-98,
where 49/50 are VCC and GND on D-SUB connector and left out"""

    parser = argparse.ArgumentParser(prog='simple line checker',
                                     description=con_schema,
                                     epilog=out_help,
                                     formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument('--port', type=str,
                        help=('serial port of mux. '
                              'If not provided, will try to connect to a serial device '
                              'with manufacturer string containing "Arduino"'))
    parser.add_argument('--port_list', action='store_true',
                        help='list all availible serial ports')
    parser.add_argument('--gnd_chan', type=int, default=1,
                        help='BNC channel (1-4) where current is shunted to ground (default 1)')
    parser.add_argument('--test_v', type=float, default=1,
                        help='test voltage to be applied (default 1 V)')
    parser.add_argument('--sens_res', type=float, default=100,
                        help='current sense resistance (default 100 Ohm)')
    parser.add_argument('--average', type=float, default=100,
                        help='how many ADC readings are averaged per resistance calculation (default 100)')
    parser.add_argument('--r_max', type=float,
                        help='drop input pins with resistances higher R_MAX')
    parser.add_argument('--r_min', type=float,
                        help='drop input pins with resistances lower R_MIN')
    parser.add_argument('--loop', action='store_true',
                        help=('refresh output continuously. '
                              'Refresh rate limited by mux communication speed, (typically 0.7sec)'))
    parser.add_argument('--map', metavar='FILE', action='append', type=str,
                        help=('translate pin numbering on D-SUB according to mapping (from->to) defined in FILE.'
                        'Expects json formated dict. '
                        'Mappings are chained in sequence if option provided multiple times. '
                        'If key is not in map, value is dropped.'))
    parser.add_argument('--outfmt', type=str, default='pinvalue',
                        help=('output format of (pin,resistance) pair. '
                        '"pinvalue": pin:resistance, "pinonly": pin only (default: pinvalue)'))
    parser.add_argument('--retain', action='store_true',
                        help='retain and output last known resistance of pin until overwritten')
    parser.add_argument('--hi', type=str, action='append',
                        help=('highlight features of output list in color. '
                        'Option can be provided multiple times. Useful with retain option. '
                              '"multi": color readings in red, '
                              'if more than one pin:resistance output per reading. '
                              '"last": make latest reading bold.'))
    parser.add_argument('--matrix', metavar='FILE',
                        help=('generate n*n resistance matrix '
                        'for each pin, where n is the number of pins and save csv-formatted to FILE. '
                        'Matrix dimensions can be reduced by using map option.'))
    parser.add_argument('--loglevel', type=str, default='WARNING',
                        help='set log messages verbosity (default WARNING)')

    args = parser.parse_args()
    logging.basicConfig(level = args.loglevel, format = '%(asctime)s:%(levelname)s:%(name)s:%(message)s')
    logger = logging.getLogger("main")


    # Read in all provided maps
    maps = []
    # Handle None if no map argument
    for map_filename in args.map or []:
        with open(map_filename, 'r',) as map_file:
            maps.append(json.loads(map_file.read()))

    mux = establish_communication(args.port, args.port_list)

    # Initial variable setup
    cmd = f'LINEPROBE:RUN {args.gnd_chan},{args.test_v},{args.sens_res},{args.average}'
    loop = True
    pin_res_retain = {} # Used to store pin resistance values for retain feature
    pin_res = parse_probe_str(query(mux, cmd)) # Query device initially to determine keys
    pin_pin_res = {} # Used to store dict of dicts for matrix output
    if args.matrix:
        if args.map:
            m_keys = list(maps[-1].keys())
        else:
            m_keys = list(pin_res.keys())
        m_keys.insert(0,'GND') # Always check connections to GND first
        m_keys_iter = iter(m_keys)
        m_key = next(m_keys_iter)
    term = Terminal()

    # Main loop
    while loop:
        pin_res = parse_probe_str(query(mux, cmd))
        if args.r_max:
            pin_res = filter_dict(pin_res, lambda v: v<=args.r_max)
        if args.r_min:
            pin_res = filter_dict(pin_res, lambda v: v>=args.r_min)
        for m in maps:
            pin_res = map_dict(pin_res, m)
        logger.info(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        if args.retain:
            #TODO: Pipe requires python >3.8.8 at least. Raise error?
            pin_res_or = pin_res_retain.copy() | pin_res
            print_dict(pin_res, retained=pin_res_or, hi=args.hi, outfmt=args.outfmt)
            pin_res_retain = pin_res_or
        else:
            print_dict(pin_res, hi=args.hi, outfmt=args.outfmt)

        if not args.loop and not args.matrix:
            loop = False
        else:
            # Handle matrix dialog
            if args.matrix:
                print(f'Press any key to measure pin {m_key}. Press "n" for next pin and "q" to quit.')
                with term.cbreak(): # Required to return as soon as input is available
                    val = term.inkey()
                if val == 'q':
                    loop = False
                if val == 'n':
                    # Prepend resistance values with pin name
                    pin_pin_res[m_key] = {'pin':m_key, **pin_res}
                    try:
                        m_key = next(m_keys_iter)
                    except StopIteration:
                        loop = False
                if not loop: # Handle csv output when exiting loop
                    if pin_pin_res['GND']:
                        pin_pin_res['GND']['GND']=0 # GND to GND has zero resistance
                    # Format dict of dicts into resistance matrix
                    for key in m_keys:
                        for (row_key,row) in pin_pin_res.items():
                            try:
                                if row_key in pin_pin_res[key]:
                                    row[key]=pin_pin_res[key][row_key]
                            except KeyError:
                                continue
                    with open(args.matrix, mode='w') as csv_output:
                        m_keys.insert(0,'pin') # Add pin name in first column
                        w = csv.DictWriter(csv_output , m_keys)
                        w.writeheader()
                        for (_,row) in pin_pin_res.items():
                            w.writerow(row)
            else:
                time.sleep(0) # Refresh rate can be slowed down here

def find_arduino(port=None, print_port_list=None):
    """Get the name of the port that is connected to Arduino."""
    if port is None:
        ports = serial.tools.list_ports.comports()
        for p in ports:
            #TODO: Autodetect does not work on Windows
            if p.manufacturer is not None and "Arduino" in p.manufacturer:
                port = p.device
            if print_port_list:
                print(f'{p.device}, {p.manufacturer}')
    return port

def query(instrument, query_str):
    """Implementation of instrument query supporting timeout."""
    logger = logging.getLogger('main')
    t0 = time.time()
    msg = query_str.strip()+WRITE_TERM
    instrument.write(msg.encode('ascii'))
    answer = b''
    timeout = time.time() + READ_TIMEOUT
    # Necessary to wait for long running commands
    while not answer:
        answer = instrument.readline().decode('ascii')
        if time.time() > timeout:
            raise RuntimeError('read operation timed out')
    if not answer.endswith(READ_TERM):
        raise RuntimeError('unexpected line termination')
    query_time = time.time()-t0
    logger.debug(f'query time: {query_time}')
    return answer.rstrip(READ_TERM)

def establish_communication(port_str, print_port_list):
    """Establish communication and check version."""
    logger = logging.getLogger('main')
    port = find_arduino(port=port_str, print_port_list=print_port_list)
    mux = serial.Serial(port, baudrate=BAUDRATE, timeout=.1)
    time.sleep(.5) # Delay required for mux initialization
    idn_str = query(mux, '*IDN?')
    logger.info(idn_str)
    # Example: AG-BLUHM, Arduino Multiplexer v1.0, 123, v1.0.1
    firmware_version = idn_str.split(',')[-1].strip().lstrip('v')
    firmware_version = version.parse(firmware_version)
    required_firmware = version.parse('1.0.1')
    if firmware_version < required_firmware: # Firmware version check
        raise FirmwareError(firmware_version, required_firmware)
    return mux

def parse_probe_str(probe_str):
    """Parse mux answer to dict."""
    pin_res = dict(item.split(':') for item in probe_str.split(','))
    pin_res = {k:float(v) for (k,v) in pin_res.items()}
    return pin_res

def filter_dict(pin_res, predicate):
    """Filter values based on conditional function predicate."""
    return {k:v for (k,v) in pin_res.items() if predicate(v)}

def map_dict(pin_res, m):
    """Apply mapping to keys. Keys that are not in m are dropped."""
    if m:
        return {str(m[str(k)]):v for (k,v) in pin_res.items() if k in m}
    else:
        return pin_res

def print_dict(pin_res, retained=None, hi=None, outfmt='pinvalue'):
    """Print out pin:resistance values seperated by comma. Supports highlighting."""
    if pin_res:
        term = Terminal()
        def key_to_num(x):
            try:
                return int(x[0])
            except ValueError:
                return x[0]
        if retained:
            sorted_lst = sorted(retained.items(), key=key_to_num)
        else:
            sorted_lst = sorted(pin_res.items(), key=key_to_num)
        out_lst = []
        for item in sorted_lst:
            pin = item[0]
            res = item[1]
            if outfmt and outfmt=='pinvalue':
                out_lst.append(f'{pin}:{res:.1f}')
            elif outfmt and outfmt=='pinonly':
                out_lst.append(f'{pin}')

            if item in pin_res.items():
                if hi and "multi" in hi:
                    if len(pin_res)>1:
                        out_lst[-1] = term.red(out_lst[-1])
                if hi and "last" in hi:
                    out_lst[-1] = term.bold(out_lst[-1])
        print(', '.join(out_lst))

if __name__ == '__main__':
    main()
