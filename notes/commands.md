
# SCPI commands

|Command|Parameter|Description|Example|
|---|---|---|---|
|*IDN?|  - |Identify the device   |-|
|*RST| - |Reset the whole switch matrix by opening all switches|-|
|MUX:STATUS?|PIN,OUT|Query the connection status between input PIN and output OUT.<br /> Return values: 0: Off (open switch);<br /> 1: On (closed switch);<br /> 128 Invalid inputs;<br /> 255: failed internal I2C communication|MUX:STATUS? 13,2|
|MUX:FULLSTATUS?|OUT|Query the connection status between all 96 inputs and one fixed output OUT.<br /> Return value: Comma separated string of 96 switch status entries| MUX:FULLSTATUS? 2|
|MUX:SWITCH:ON|PIN,OUT|Connect input PIN to output OUT<br /> Return values:<br /> 0: Successful connection;<br /> 128: Invalid inputs; <br />Otherwise: Failed internal I2C communication|MUX:SWITCH:ON 13,2|
|MUX:SWITCH:OFF|PIN,OUT|Disconnect input PIN to output OUT<br /> Return values:<br /> 0: Successful disconnection;<br /> 128: Invalid inputs;<br /> Otherwise: Failed internal I2C communication|MUX:SWITCH:OFF 13,2||
|DAC:SETV|Voltage [0.55V,2.75V]|Set voltage on inbuilt 12 bit DAC  Return values: 12 bit binary word written to the DAC  | DAC:SETV 2.1|
|DAC:GETV?|-| Read voltage set on the inbuilt 12 bit DAC Return values: Voltage between 0.55V and 2.75V Note: This is not a measured value, instead it is the last set voltage to the DAC using SETV command|DAC:GETV?|
|ADC:GETV?|-|Read voltage value from the 12 bit inbuilt ADC Return values: Voltage between GND and 3.3V|ADC:GETV?|
|ADC:AVERAGE?|N_Samples|Read N_Samples times averaged voltage value from the 12 bit inbuilt ADC Return values: Voltage between GND and 3.3V|ADC:AVERAGE? 100|
|LINEPROBE:RUN|GND, V_DAC, R_fix|Quickly measure resistance between fixed probe and all 96 input lines<br /> Return values: Comma separated string of 96 resistance values between probe and input lines|LINEPROBE:RUN 2,1.0,100|


# QCoDeS commands

The individual commands have standard get/set arithmetic corresponding to other QCoDeS instruments to seemlessly fit into QCoDeS measurement protocols.

|Command|Parameter|Description|Example|
|---|---|---|---|
|Cxx_0y|xx = 01,02,...48<br /> xx = 51,52,...98<br /> y = 1,2,3,4|96x4 parameters each corresponding to the connection between input xx and output 0y|Cxx_0y.get(): 0 (open switch), 1 (closed switch)<br /> Cxx_0y.set(0): Open switch|
|output_channel_y|y = 1,2,3,4|4 parameters each corresponding to one output channel| output_channel_y.get(): list of input pins connected to output channel y<br /> output_channel_y.set(list of pins): connect input pins in list to output channel y without changing previous connections|
|input_channel_xx|xx = 01,02,...48<br /> xx = 51,52,...98|Parameters corresponding to the input channel.<br /> Connect multiple output channels to one input pin xx (or query the same)|channel_xx.get(): list of output channels connected to input pin<br /> channel_xx.set(list of channels): connect output channels in list to input pin xx without changing previous connections|
|DAC|-|Get and set voltage of inbuilt DAC|DAC.get(): Get latest set voltage<br /> DAC.set(voltage): Set DAC voltage between 0.55 and 2.75V|
|ADC|-|Read voltage at inbuilt ADC|ADC.get(): get voltage from inbuilt ADC (between 0 and 3.3V)|
